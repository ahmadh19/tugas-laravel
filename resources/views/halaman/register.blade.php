@extends('layout.master')

@section('judul')
Registrasi    
@endsection

@section('content')
<h1>Buar Account Baru</h1>
<h3>Sign Up Form </h3>
<form action="/kirim" method="post">
    @csrf
    <label for="fname">First name:</label><br><br>
    <input type="text" id="fname" name="fname" ><br><br>

    <label for="lname">Last name:</label><br><br>
    <input type="text" id="lname" name="lname"><br><br>

    <label for="gender">Gender : </label><br>
    <input type="radio" id="male" name="fav_language">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="fav_language">
    <label for="female">Female</label><br><br>

    <label for="nationality">Nationality</label> <br>
    <select id="nationality" name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="brunei">Brunei Darussalam</option>
        <option value="singapore">Singapore</option>
    </select>
    <br><br>

    <label for="language">Languafe for Spoken</label> <br>
    <input type="checkbox" id="indonesia" name="indonesia">
    <label for="indonesia"> Bahasa Indonesia</label><br>
    <input type="checkbox" id="english" name="english">
    <label for="english"> English </label><br>
    <input type="checkbox" id="other" name="other">
    <label for="other"> Other </label>
    <br><br>

    <label for="bio"> Bio </label><br>
    <textarea name="bio" rows="10" cols="20"></textarea>
    <br><br>

    <input type="submit" value="Sign Up">

</form>

@endsection
    
